package com.stawirej.automatictransmission;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

class AutomaticTransmissionControllerSportModeScenarios {

    private AutomaticTransmissionController.DrivingMode MODE = AutomaticTransmissionController.DrivingMode.SPORT;

    private double RAPID_ACCELERATION = 0.7;
    private double NO_ACCELERATION = 0.1;

    private int LOW_RPM = 1500;
    private int NORMAL_RPM = 2500;

    @Test
    void should() {
        // given

        // when

        // then
    }


    @Test
    void shouldReduceGearWhenAcceleratingRapidlyOnLowRPMOnFlatRoad() {
        //given
        int currentGear = 5;

        AutomaticGearBox gearBox = mock(AutomaticGearBox.class);
        when(gearBox.getGear()).thenReturn(currentGear);

        EngineMonitoringSystem engineMonitoringSystem = mock(EngineMonitoringSystem.class);
        when(engineMonitoringSystem.getCurrentRPM()).thenReturn(LOW_RPM);

        Navigation navigation = mock(Navigation.class);
        when(navigation.getCurrentSlope()).thenReturn(Slope.Flat);
        when(navigation.getNextSlope()).thenReturn(Slope.Flat);

        AutomaticTransmissionController controller = new AutomaticTransmissionController(
            MODE,
            gearBox,
            engineMonitoringSystem,
            navigation);

        //when
        controller.handleGas(RAPID_ACCELERATION);

        //then
        int desiredGear = currentGear - 1;
        verify(gearBox, times(1)).changeGear(desiredGear);
    }

    @Test
    void shouldReduceGearWhenUphillRideAhead() {
        //given
        int currentGear = 4;

        AutomaticGearBox gearBox = mock(AutomaticGearBox.class);
        when(gearBox.getGear()).thenReturn(currentGear);

        EngineMonitoringSystem engineMonitoringSystem = mock(EngineMonitoringSystem.class);
        when(engineMonitoringSystem.getCurrentRPM()).thenReturn(NORMAL_RPM);

        Navigation navigation = mock(Navigation.class);
        when(navigation.getCurrentSlope()).thenReturn(Slope.Flat);
        when(navigation.getNextSlope()).thenReturn(Slope.Positive);

        AutomaticTransmissionController controller = new AutomaticTransmissionController(
            MODE,
            gearBox,
            engineMonitoringSystem,
            navigation);

        //when
        controller.handleGas(NO_ACCELERATION);

        //then
        int desiredGear = currentGear - 1;
        verify(gearBox, times(1)).changeGear(desiredGear);
    }
}
