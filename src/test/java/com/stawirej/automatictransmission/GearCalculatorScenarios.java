package com.stawirej.automatictransmission;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class GearCalculatorScenarios {

    private double RAPID_ACCELERATION = 0.7;
    private double NO_ACCELERATION = 0.1;

    private int LOW_RPM = 1500;
    private int NORMAL_RPM = 2500;

    Object[] prepareParamsForGearCalculator() {
        return new Object[] {
            new Object[] {5, 4, LOW_RPM, Slope.Flat, Slope.Flat},
            new Object[] {4, 3, NORMAL_RPM, Slope.Flat, Slope.Positive},
            new Object[] {3, 2, NORMAL_RPM, Slope.Flat, Slope.Positive},
        };
    }

    @Test
    @Parameters(method = "prepareParamsForGearCalculator")
    public void shouldReduceGearWhenAcceleratingRapidlyOnLowRPMOnFlatRoad(final int currentGear,
                                                                          final int desiredGear,
                                                                          final int currentRPM,
                                                                          final Slope currentSlope,
                                                                          final Slope nextSlope) {
        //given
        GearCalculator gearCalculator = new SimpleGearCalculator();

        // when
        int returnedGear = gearCalculator.calculateGear(currentGear, currentRPM, currentSlope, nextSlope);

        //then
        then(returnedGear).isEqualTo(desiredGear);
    }

}
