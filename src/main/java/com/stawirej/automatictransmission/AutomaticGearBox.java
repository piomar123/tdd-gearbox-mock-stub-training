package com.stawirej.automatictransmission;

/**
 * Executive module of the gear box
 */
public interface AutomaticGearBox {

    /**
     * @return maximum forward gears supported by this model
     */
    int getMaxGears();

    /**
     * changes forward gear
     *
     * @param gear
     */
    void changeGear(int gear);

    /**
     * @return current gear
     */
    int getGear();

    /**
     * changes gear box mode to D (drive)
     */
    void drive();

    /**
     * changes gear box mode to P (park)
     */
    void park();

    /**
     * changes gear box mode to N (neutral)
     */
    void neutral();

    /**
     * changes gear box mode to R (reverse)
     */
    void reverse();
}
