package com.stawirej.automatictransmission;

/**
 * Controlling module of the gear box
 */
public class AutomaticTransmissionController {

    public enum DrivingMode {
        ECO,
        COMFORT,
        SPORT,
        SPORT_PLUS
    }

    private AutomaticGearBox gearBox;
    private EngineMonitoringSystem engineMonitoringSystem;
    private DrivingMode drivingMode;
    private Navigation navigation;

    public AutomaticTransmissionController(DrivingMode initialDrivingMode, AutomaticGearBox gearBox, EngineMonitoringSystem engineMonitoringSystem,
                                           Navigation navigation) {
        this.drivingMode = initialDrivingMode;
        this.gearBox = gearBox;
        this.engineMonitoringSystem = engineMonitoringSystem;
        this.navigation = navigation;
    }

    public void start() {
        //TODO check engine and start gearbox
    }

    public void stop() {
        //TODO park gearbox
    }

    public void changeMode(DrivingMode drivingMode) {
        this.drivingMode = drivingMode;
        handleChange(0, 0);
    }

    public void handleGas(double throttle) {
        handleChange(throttle, 0);
    }

    public void handleBreaks(double breakingForce) {
        handleChange(0, breakingForce);
    }

    /**
     * maintains gear that is proper for current:
     * driving mode, engine's RPM
     * and given gas throttle and breaking force
     *
     * @param throttle      gas throttle <0,1>
     * @param breakingForce breaking force <0,1>
     */
    private void handleChange(double throttle, double breakingForce) {
        //TODO check if we are actually started (Driving Mode)

        int gear = calculateGear(throttle, breakingForce);

        if (gear != gearBox.getGear()) {
            this.gearBox.changeGear(gear);
        }
    }

    /**
     * determines gear that is proper for current:
     * driving mode, engine's RPM
     * and given gas throttle and breaking force
     *
     * @param throttle      gas throttle <0,1>
     * @param breakingForce breaking force <0,1>
     * @return optimal gear
     */
    private int calculateGear(double throttle, double breakingForce) {
        // int currentRPM = engineMonitoringSystem.getCurrentRPM();
        // Slope currentSlope = navigation.getCurrentSlope();
        // Slope nextSlope = navigation.getNextSlope();
        // // TODO maybe use Strategy Design Pattern/Template Method pattern to handle different driving modes
        //
        // final RoadType roadType = checkRoadType(currentSlope, nextSlope);
        // final RPMLevel rpmLevel = checkRPMLevel(currentRPM);
        //
        // if (rpmLevel.equals(RPMLevel.Low) && roadType == RoadType.Flat) {
        //     return gearBox.getGear() - 1;
        // }
        // else if(rpmLevel.equals(RPMLevel.Medium) && roadType == RoadType.Up) {
        //     return gearBox.getGear() - 1;
        // }else{
        //     return gearBox.getGear();
        // }
        return 0;
    }


}
