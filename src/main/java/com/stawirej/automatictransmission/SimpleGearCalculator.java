package com.stawirej.automatictransmission;

public class SimpleGearCalculator implements GearCalculator {

    private int LOW_RPM = 1500;
    private int NORMAL_RPM = 2500;

    private double RAPID_ACCELERATION = 0.7;
    private double NO_ACCELERATION = 0.1;

    @Override
    public int calculateGear(final int currentGear, final int currentRPM, final Slope currentSlope, final Slope nextSlope) {
        final RoadType roadType = checkRoadType(currentSlope, nextSlope);
        final RPMLevel rpmLevel = checkRPMLevel(currentRPM);

        if (rpmLevel.equals(RPMLevel.Low) && roadType == RoadType.Flat) {
            return currentGear - 1;
        } else if (rpmLevel.equals(RPMLevel.Medium) && roadType == RoadType.Up) {
            return currentGear - 1;
        } else {
            return currentGear;
        }
    }

    private RPMLevel checkRPMLevel(int currentRPM) {
        if (currentRPM <= LOW_RPM) {
            return RPMLevel.Low;
        } else if (currentRPM > NORMAL_RPM) {
            return RPMLevel.High;
        } else {
            return RPMLevel.Medium;
        }
    }

    private RoadType checkRoadType(final Slope currentSlope, final Slope nextSlope) {
        if (currentSlope == Slope.Flat && nextSlope == Slope.Flat) {
            return RoadType.Flat;
        } else if (currentSlope == Slope.Flat && nextSlope == Slope.Positive) {
            return RoadType.Up;
        }
        throw new RuntimeException("Unknown road type");
    }
}
