package com.stawirej.automatictransmission;

/**
 * Engine's monitoring module 
 */
public interface EngineMonitoringSystem {

	/**
	 * 
	 * @return engine's revolutions per minute
	 */
	public int getCurrentRPM();
	
	public boolean isOn();

}
