package com.stawirej.automatictransmission;


/**
 * Slopes example:
 *
 *       /\   _ _
 *     _/  \/    \_ _
 *
 * Flat,Positive,Positive,Negative,Negative,Positive,Flat,Flat,Negative,Flat,Flat
 *
 */
public enum Slope {
    Positive,
    Negative,
    Flat
}
