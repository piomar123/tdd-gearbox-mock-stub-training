package com.stawirej.automatictransmission;

public enum RPMLevel {
    Low,
    Medium,
    High
}
