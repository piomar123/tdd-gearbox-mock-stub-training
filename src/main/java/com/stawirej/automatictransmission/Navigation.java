package com.stawirej.automatictransmission;

public interface Navigation {

    Slope getCurrentSlope();

    Slope getNextSlope();
}
