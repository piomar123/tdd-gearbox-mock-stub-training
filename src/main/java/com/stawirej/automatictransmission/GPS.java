package com.stawirej.automatictransmission;


public interface GPS {
    long getLongitude();

    long getLatitude();
}
