package com.stawirej.automatictransmission;

public interface GearCalculator {
    int calculateGear(int currentGear, int currentRPM, Slope currentSlope, Slope nextSlope);
}
